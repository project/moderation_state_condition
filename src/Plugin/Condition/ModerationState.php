<?php

namespace Drupal\moderation_state_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Moderation State' condition.
 *
 * @Condition(
 *   id = "moderation_state_condition_moderation_state",
 *   label = @Translation("Moderation State"),
 *   context_definitions = {
 *     "node" = @ContextDefinition(
 *       "entity:node",
 *        label = @Translation("Node")
 *      )
 *   }
 * )
 */
class ModerationState extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new ModerationState instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['moderation_states' => []] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Options to be used for the form.
    $states = [];

    // Get all the existing workflows for content moderation.
    $workflows = $this->entityTypeManager->getStorage("workflow")
      ->loadByProperties(['type' => 'content_moderation']);

    $multiple = count($workflows) > 1;

    // Get the keys and labels for all moderation states.
    foreach ($workflows as $workflow) {
      $type_settings = $workflow->get('type_settings');
      $workflow_label = $workflow->get('label');

      foreach ($type_settings['states'] as $type => $settings) {
        // If there are multiple workflows, use the moderation state label
        // and workflow label to make each option clear as
        // to which workflow it uses.
        if ($multiple) {
          $label = $settings['label'] . ' (' . $workflow_label . ')';
        }
        else {
          $label = $settings['label'];
        }

        // If there is already a key that exists for that type,
        // add to the existing label.
        if (!empty($states[$type]) && $states[$type] !== $label) {
          $states[$type] = $states[$type] . ', ' . $label;
        }
        else {
          $states = $states + [$type => $label];
        }
      }
    }

    $form['moderation_states'] = [
      '#title' => $this->t('Moderation State'),
      '#type' => 'checkboxes',
      '#default_value' => $this->configuration['moderation_states'],
      '#options' => $states,
      '#description' => $this->t('If you select no states, the condition will evaluate to TRUE for all states.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['moderation_states'] = array_filter($form_state->getValue('moderation_states'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $moderation_states = array_values($this->configuration['moderation_states']);

    if (count($moderation_states) > 1) {
      $moderation_states = implode(', ', $moderation_states);
    }
    else {
      $moderation_states = reset($moderation_states);
    }

    if (!empty($this->configuration['negate'])) {
      return $this->t('The moderation state is not @states', ['@states' => $moderation_states]);
    }
    else {
      return $this->t('The moderation state is @states', ['@states' => $moderation_states]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['moderation_states']) && !$this->isNegated()) {
      return TRUE;
    }

    $node = $this->getContextValue('node');
    $moderation_state = $node->get('moderation_state')->getString();

    return in_array($moderation_state, $this->configuration['moderation_states']);
  }

}
